﻿Console.WriteLine("Nhap so quang vang ban thu duoc: ");
int goldAcess = int.Parse(Console.ReadLine());
int firstGold = Math.Min(goldAcess, 10);
int price10gold = firstGold * 10;
goldAcess -= firstGold;

int secondGold = Math.Min(goldAcess, 5);
int price5gold = secondGold * 5;
goldAcess -= secondGold;

int thirdGold = Math.Min(goldAcess, 3);
int price3gold = thirdGold * 2;
goldAcess -= thirdGold;

int lastOre = Math.Min(goldAcess, 11);
int lastPrice = lastOre * 1;
goldAcess -= lastOre;
int totalprice = (price10gold + price5gold + price3gold + lastPrice);
Console.WriteLine("Total money you received: " + totalprice);

